# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import zlib

from flask import current_app
from flask import json
from itsdangerous import base64_decode

import kadi.lib.constants as const


def _get_session_cookie(client):
    cookie = client.get_cookie(current_app.config["SESSION_COOKIE_NAME"])
    cookie_data = cookie.value
    compressed = False

    if cookie_data.startswith("."):
        cookie_data = cookie_data[1:]
        compressed = True

    cookie_data = base64_decode(cookie_data.split(".")[0])

    if compressed:
        cookie_data = zlib.decompress(cookie_data)

    return json.loads(cookie_data.decode())


def test_session_cookie(client, user_session):
    """Test if the session cookie is set correctly."""
    client.get("/records")
    session_cookie = _get_session_cookie(client)

    assert len(session_cookie) == 2

    # Flash messages from Flask and the redirect URL.
    for key in ["_flashes", const.SESSION_KEY_NEXT_URL]:
        assert key in session_cookie

    with user_session():
        client.get("/")
        session_cookie = _get_session_cookie(client)

        assert len(session_cookie) == 4

        # Flask-Login values and the CSRF token from Flask-WTF (due to the user session
        # fixture performing a POST request).
        for key in ["_id", "_fresh", "_user_id", "csrf_token"]:
            assert key in session_cookie

    session_cookie = _get_session_cookie(client)

    assert len(session_cookie) == 1
    # Only the CSRF token should remain.
    assert "csrf_token" in session_cookie
