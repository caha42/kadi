# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import kadi.lib.constants as const
from kadi.lib.permissions.core import add_role
from kadi.lib.permissions.models import Permission
from kadi.lib.permissions.models import Role
from kadi.lib.permissions.models import RoleRule
from kadi.lib.permissions.models import RoleRuleType
from kadi.lib.permissions.utils import create_username_role_rule
from kadi.lib.permissions.utils import get_group_roles
from kadi.lib.permissions.utils import get_role_rules
from kadi.lib.permissions.utils import get_user_roles
from kadi.lib.permissions.utils import initialize_system_role
from kadi.lib.web import url_for
from kadi.modules.accounts.models import User
from kadi.modules.groups.models import Group
from tests.utils import check_view_response


def test_permission_required(client, dummy_record, new_user, user_session):
    """Test if the "permission_required" decorator works correctly."""
    username = password = "test"
    user = new_user(username=username, password=password)

    with user_session(username=username, password=password):
        # Unpermitted request.
        response = client.get(url_for("records.view_record", id=dummy_record.id))
        check_view_response(response, status_code=403)

        # Permitted request.
        add_role(user, "record", dummy_record.id, "member")

        response = client.get(url_for("records.view_record", id=dummy_record.id))
        check_view_response(response)

        # Invalid object ID.
        response = client.get(url_for("records.view_record", id=dummy_record.id + 1))
        check_view_response(response, status_code=404)


def test_initialize_system_role(new_user):
    """Test if system roles are initialized correctly."""
    for role_name in const.SYSTEM_ROLES:
        # The system roles should already be initialized via the database fixtures.
        assert not initialize_system_role(role_name)

    for name, permissions in const.SYSTEM_ROLES.items():
        role = Role.query.filter_by(name=name, object=None, object_id=None).one()

        for object_name, actions in permissions.items():
            for action in actions:
                permission = Permission.query.filter_by(
                    action=action, object=object_name, object_id=None
                ).one()

                assert permission in role.permissions


def test_get_user_roles(dummy_record, dummy_user):
    """Test if determining user roles works correctly."""
    user_role = get_user_roles("record").one()

    assert isinstance(user_role[0], User)
    assert isinstance(user_role[1], Role)

    assert user_role[0] == dummy_user
    assert user_role[1].object_id == dummy_record.id

    user_role = get_user_roles("record", object_id=dummy_record.id).one()

    assert user_role[0] == dummy_user
    assert user_role[1].object_id == dummy_record.id


def test_get_group_roles(dummy_group, dummy_record):
    """Test if determining group roles works correctly."""
    add_role(dummy_group, "record", dummy_record.id, "member")
    group_role = get_group_roles("record").one()

    assert isinstance(group_role[0], Group)
    assert isinstance(group_role[1], Role)

    assert group_role[0] == dummy_group
    assert group_role[1].object_id == dummy_record.id

    group_role = get_group_roles("record", object_id=dummy_record.id).one()

    assert group_role[0] == dummy_group
    assert group_role[1].object_id == dummy_record.id


def test_create_username_role_rule(db, dummy_record):
    """Test if username role rules are created correctly."""

    # Try to create an invalid username role rules using an invalid identity type.
    assert (
        create_username_role_rule("record", dummy_record.id, "member", "test", "*")
        is None
    )

    # Try to create a valid username role rule.
    identity_type = const.AUTH_PROVIDER_TYPE_LOCAL

    role_rule = create_username_role_rule(
        "record", dummy_record.id, "member", identity_type, "*"
    )
    db.session.commit()

    assert RoleRule.query.filter_by(type=RoleRuleType.USERNAME).one() == role_rule
    assert role_rule.condition == {"identity_type": identity_type, "pattern": "*"}


def test_get_role_rules(db, dummy_record):
    """Test if role rules are retrieved correctly."""
    role_rule = create_username_role_rule(
        "record", dummy_record.id, "member", const.AUTH_PROVIDER_TYPE_LOCAL, "*"
    )
    db.session.commit()

    assert get_role_rules("record", dummy_record.id).one() == role_rule
    assert (
        get_role_rules("record", dummy_record.id, rule_type=RoleRuleType.USERNAME).one()
        == role_rule
    )
