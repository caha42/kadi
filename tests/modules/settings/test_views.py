# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from urllib.parse import unquote

import pytest
from flask import current_app
from flask import get_flashed_messages
from flask import json

import kadi.lib.constants as const
from kadi.lib.api.models import PersonalToken
from kadi.lib.config.core import get_user_config
from kadi.lib.oauth.core import create_oauth2_client_token
from kadi.lib.oauth.models import OAuth2ClientToken
from kadi.lib.oauth.models import OAuth2ServerClient
from kadi.lib.web import url_for
from tests.utils import check_view_response


def test_edit_profile_confirm_email(client, user_session):
    """Test the "confirm_email" action of the "settings.edit_profile" endpoint."""
    endpoint = url_for("settings.edit_profile")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(url_for("settings.edit_profile", action="confirm_email"))

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert "A confirmation email has been sent." in get_flashed_messages()


@pytest.mark.parametrize("email_required", [True, False])
def test_edit_profile_edit(
    monkeypatch, email_required, client, dummy_user, user_session
):
    """Test the regular "settings.edit_profile" endpoint."""
    monkeypatch.setitem(
        current_app.config["AUTH_PROVIDERS"][const.AUTH_PROVIDER_TYPE_LOCAL],
        "email_confirmation_required",
        email_required,
    )

    new_email = "test@example.com"
    flash_msg = "A confirmation email has been sent."
    endpoint = url_for("settings.edit_profile")

    dummy_user.identity.email_confirmed = True

    with user_session():
        response = client.post(endpoint, data={"email": new_email})

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert dummy_user.identity.email == new_email
        assert not dummy_user.identity.email_confirmed

        if email_required:
            assert flash_msg in get_flashed_messages()
        else:
            assert flash_msg not in get_flashed_messages()


def test_change_password(client, dummy_user, user_session):
    """Test the "settings.change_password" endpoint."""
    endpoint = url_for("settings.change_password")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "password": dummy_user.identity.username,
                "new_password": "test1234",
                "new_password2": "test1234",
            },
        )

        check_view_response(response, status_code=302)
        assert dummy_user.identity.check_password("test1234")


def test_manage_preferences_customization(client, dummy_user, user_session):
    """Test the "customization" tab of the "settings.manage_preferences" endpoint."""
    endpoint = url_for("settings.manage_preferences", tab="customization")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={"home_layout": json.dumps(const.USER_CONFIG_HOME_LAYOUT_DEFAULT)},
        )

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert (
            get_user_config(const.USER_CONFIG_HOME_LAYOUT, user=dummy_user)
            == const.USER_CONFIG_HOME_LAYOUT_DEFAULT
        )


def test_manage_preferences_plugins(client, dummy_user, user_session):
    """Test the "plugins" tab of the "settings.manage_preferences" endpoint."""
    endpoint = url_for("settings.manage_preferences", tab="plugins", plugin="influxdb")
    data = [{"name": "test", "token": "test", "title": "test"}]

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"influxdbs": json.dumps(data)})

        check_view_response(response, status_code=302)
        assert response.location == endpoint
        assert (
            get_user_config("INFLUXDB_INFLUXDBS", user=dummy_user, decrypt=True) == data
        )


def test_manage_personal_tokens(client, user_session):
    """Test the "settings.manage_personal_tokens" endpoint."""
    endpoint = url_for("settings.manage_personal_tokens")

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"name": "test"})

        check_view_response(response)
        assert PersonalToken.query.filter_by(name="test").one()


def test_manage_applications(client, user_session):
    """Test the "settings.manage_applications" endpoint."""
    endpoint = url_for("settings.manage_applications")
    client_name = "test"

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "client_name": client_name,
                "client_uri": "https://foo.bar",
                "redirect_uris": "https://foo.bar",
            },
        )

        check_view_response(response)
        assert OAuth2ServerClient.query.one().client_name == client_name


def test_edit_application(client, dummy_oauth2_server_client, user_session):
    """Test the "settings.edit_application" endpoint."""
    new_client_name = f"{dummy_oauth2_server_client}-test"
    endpoint = url_for("settings.edit_application", id=dummy_oauth2_server_client.id)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"client_name": new_client_name})

        check_view_response(response, status_code=302)
        assert dummy_oauth2_server_client.client_name == new_client_name


def test_delete_application(client, db, dummy_oauth2_server_client, user_session):
    """Test the "settings.delete_application" endpoint."""
    with user_session():
        response = client.post(
            url_for("settings.delete_application", id=dummy_oauth2_server_client.id)
        )
        db.session.commit()

        check_view_response(response, status_code=302)
        assert not OAuth2ServerClient.query.all()


def test_manage_services(client, db, dummy_user, user_session):
    """Test the "settings.manage_services" endpoint."""
    provider_name = "zenodo"
    endpoint = url_for("settings.manage_services")

    create_oauth2_client_token(name=provider_name, access_token="test", user=dummy_user)
    db.session.commit()

    assert OAuth2ClientToken.query.one()

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(f"{endpoint}?disconnect={provider_name}")
        db.session.commit()

        check_view_response(response, status_code=302)
        assert not OAuth2ClientToken.query.all()


def test_oauth2_provider_login(client, user_session):
    """Test the "settings.oauth2_provider_login" endpoint."""
    provider_name = "zenodo"
    redirect_uri = url_for("settings.oauth2_provider_authorize", provider=provider_name)

    with user_session():
        response = client.get(
            url_for("settings.oauth2_provider_login", provider=provider_name)
        )

        check_view_response(response, status_code=302)
        assert f"redirect_uri={redirect_uri}" in unquote(response.location)


def test_oauth2_provider_authorize(client, user_session):
    """Test the "settings.oauth2_provider_authorize" endpoint."""
    with user_session():
        response = client.get(
            url_for("settings.oauth2_provider_authorize", provider="zenodo")
        )

        check_view_response(response, status_code=302)
        assert "Error connecting service." in get_flashed_messages()
