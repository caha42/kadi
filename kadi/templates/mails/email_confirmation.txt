{% trans %}Hello {{ displayname }}{% endtrans %},

{% trans %}to confirm your email address, please paste the following link into your browser's address bar:{% endtrans %}
{{ url_for("accounts.confirm_email", token=token) }}

{% trans duration=expires_in | duration %}
Note that this link is only valid for a duration of {{ duration }}.
{% endtrans %}
